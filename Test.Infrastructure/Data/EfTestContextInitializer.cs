﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Core.Model.OrderAggregate;
using Test.Core.Model.KundAggregate;
using Test.Core.Model.ProduktAggregate;

namespace Test.Infrastructure.Data
{
	public class EfTestContextInitializer : DropCreateDatabaseIfModelChanges<EfTestContext>
	{
		private List<Produkt> produktLista = new List<Produkt>();
		private List<Kund> kundLista = new List<Kund>();
		private List<BetalningsTyp> betalningsTypLista = new List<BetalningsTyp>();
		private List<BetalningsMetod> betalningsMetodLista = new List<BetalningsMetod>();
		private List<LeveransMetod> leveransMetodLista = new List<LeveransMetod>();

		public EfTestContextInitializer()
		{
			produktLista.AddRange( new [] {
				new Produkt { Namn = "Pryl", Tillverkare = "Acme", Pris = 9999.90m },
				new Produkt { Namn = "Pillimoj", Tillverkare = "Stark Industries", Pris = 55 },
				new Produkt { Namn = "Sak", Tillverkare = "Very Big Corp. of America", Pris = 159762.45m },
				new Produkt { Namn = "Mackapär", Tillverkare = "Sirius Cybernetics Corp.", Pris = 1359.90m}
			});

			kundLista.Add(Kund.Create("Kalle", "Karlsson", "Ödemarksstigen 51", "", "Korpilombolo", "12345", "071-987357", "kalle.karlsson@banzai.se", "kalle", "abc123"));

			var kort = new BetalningsTyp {Namn = "Kort"};
			var postförskott = new BetalningsTyp {Namn = "Postförskott", Datafält = "bankgironummer"};
			var bank = new BetalningsTyp {Namn = "Bank", Datafält = "clearingnummer"};
			betalningsTypLista.AddRange( new[] { kort, postförskott, bank });

			betalningsMetodLista.AddRange(new[] {
				new BetalningsMetod {Namn =  "Visa", Data = "1234", BetalningsTyp = kort },
				new BetalningsMetod {Namn =  "Diners Club", Data = "2345", BetalningsTyp = kort },
				new BetalningsMetod {Namn =  "American Express", Data = "3456", BetalningsTyp = kort },
				new BetalningsMetod {Namn =  "Postförskott", Data = "9632-1478", BetalningsTyp = postförskott },
				new BetalningsMetod {Namn =  "Nordea", Data = "1478", BetalningsTyp = bank },
				new BetalningsMetod {Namn =  "Swebank", Data = "2846", BetalningsTyp = bank },
				new BetalningsMetod {Namn =  "SEB", Data = "5713", BetalningsTyp = bank },
				new BetalningsMetod {Namn =  "Handelsbanken", Data = "9175", BetalningsTyp = bank },
			});


			leveransMetodLista.AddRange(new[] {
				new LeveransMetod {Namn="Posten", Kostnad=25, LeveransDagar=3 },
				new LeveransMetod {Namn="DHL", Kostnad=12, LeveransDagar=5 },
				new LeveransMetod {Namn="DB Shenker", Kostnad=40, LeveransDagar=1 }
			});
		}

		protected override void Seed(EfTestContext context)
		{
			foreach (var p in produktLista)
				context.ProduktCtx.Add(p);

			foreach (var k in kundLista)
				context.KundCtx.Add(k);

			foreach (var bt in betalningsTypLista)
				context.BetalningsTypCtx.Add(bt);

			foreach (var bm in betalningsMetodLista)
				context.BetalningsMetodCtx.Add(bm);

			foreach (var lm in leveransMetodLista)
				context.LeveransMetodCtx.Add(lm);

			context.SaveChanges();
			base.Seed(context);
		}
	}
}
