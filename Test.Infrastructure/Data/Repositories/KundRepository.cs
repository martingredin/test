﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Core.Model.KundAggregate;
using Test.Core.Repositories;
using Test.SharedKernel.Data;

namespace Test.Infrastructure.Data.Repositories
{
	public class KundRepository : EFRepository<Kund>, IKundRepository
	{
		protected EfTestContext EfTestContext
		{
			get { return DataContext as EfTestContext; }
		}

		public KundRepository(EfTestContext dbContext)
			: base(dbContext)
		{

		}

		public Kund GetByAnvändarId(string användarId)
		{
			return (from k in EfTestContext.KundCtx
					where användarId == k.AnvändarId
					select k).FirstOrDefault();
		}
	}
}
