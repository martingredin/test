﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Core.Model.OrderAggregate;
using Test.Core.Repositories;
using Test.SharedKernel.Data;

namespace Test.Infrastructure.Data.Repositories
{
	public class OrderRepository : EFRepository<Order>, IOrderRepository
	{
		protected EfTestContext EfTestContext
		{
			get { return DataContext as EfTestContext; }
		}

		public OrderRepository(EfTestContext dbContext)
			: base(dbContext)
		{

		}

		public IEnumerable<OrderRad> GetOrderRaderByOrderId(int orderId)
		{
			var orderRader = (from o in EfTestContext.OrderRadCtx
							  where o.OrderId == orderId
							  select o).ToList();

			return orderRader ?? null;
		}

		public Order GetOrderHuvudByOrderRadId(int orderRadId)
		{
			var order = (from or in EfTestContext.OrderRadCtx
						 where or.Id == orderRadId
						 select or.Order).FirstOrDefault();

			return order ?? null;
		}

		public void Add(Order orderHuvud, IEnumerable<OrderRad> orderRader)
		{
			this.EfTestContext.OrderRadCtx.AddRange(orderRader);
			this.EfTestContext.OrderCtx.Add(orderHuvud);
			this.EfTestContext.SaveChanges();
		}

		public IEnumerable<BetalningsTyp> GetBetalningsTyper()
		{
			var betalningsTyper = (from bt in EfTestContext.BetalningsTypCtx
								   select bt).ToList();

			return betalningsTyper;
		}

		public BetalningsMetod GetBetalningsMetod(int betalningsMetodId)
		{
			var betalningsMetod = (from bm in EfTestContext.BetalningsMetodCtx
								   where bm.Id == betalningsMetodId
								   select bm).FirstOrDefault();

			return betalningsMetod;
		}

		public IEnumerable<BetalningsMetod> GetBetalningsMetoder()
		{
			var betalningsMetoder = (from bm in EfTestContext.BetalningsMetodCtx
									 select bm).ToList();

			return betalningsMetoder;
		}

		public LeveransMetod GetLeveransMetod(int leveransMetodId)
		{
			var leveransMetod = (from lm in EfTestContext.LeveransMetodCtx
								 where lm.Id == leveransMetodId
								 select lm).FirstOrDefault();

			return leveransMetod;
		}

		public IEnumerable<LeveransMetod> GetLeveransMetoder()
		{
			var leveransMetoder = (from lm in EfTestContext.LeveransMetodCtx
								   select lm).ToList();

			return leveransMetoder;
		}
	}
}
