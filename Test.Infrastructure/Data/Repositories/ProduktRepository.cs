﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Core.Model.ProduktAggregate;
using Test.Core.Repositories;
using Test.SharedKernel.Data;

namespace Test.Infrastructure.Data.Repositories
{
	public class ProduktRepository : EFRepository<Produkt>, IProduktRepository 
	{
		protected EfTestContext EfTestContext
		{
			get { return DataContext as EfTestContext; }
		}

		public ProduktRepository(EfTestContext dbContext)
			: base(dbContext)
		{

		}
	}
}