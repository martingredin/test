﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Test.Core.Model;
using Test.Core.Model.KundAggregate;
using Test.Core.Model.ProduktAggregate;
using Test.Core.Model.OrderAggregate;

namespace Test.Infrastructure.Data
{
	public class EfTestContext : DbContext
	{
		public DbSet<Kund> KundCtx { get; set; }
		public DbSet<Produkt> ProduktCtx { get; set; }
		public DbSet<Order> OrderCtx { get; set; }
		public DbSet<OrderRad> OrderRadCtx { get; set; }
		public DbSet<Betalning> BetalningCtx { get; set; }
		public DbSet<BetalningsTyp> BetalningsTypCtx { get; set; }
		public DbSet<BetalningsMetod> BetalningsMetodCtx { get; set; }
		public DbSet<LeveransMetod> LeveransMetodCtx { get; set; }

		private const string dbName = "TestDb";

		public EfTestContext()
			: base(dbName)
		{
			Database.SetInitializer<EfTestContext>(new EfTestContextInitializer());
		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder
				.Entity<Kund>()
				.ToTable("Kund");

			modelBuilder
				.Entity<Produkt>()
				.ToTable("Produkt");

			modelBuilder
				.Entity<Order>()
				.ToTable("Order");

			modelBuilder
				.Entity<OrderRad>()
				.ToTable("OrderRad");

			modelBuilder
				.Entity<Betalning>()
				.ToTable("Betalning");

			modelBuilder
				.Entity<BetalningsTyp>()
				.ToTable("BetalningsTyp");

			modelBuilder
				.Entity<BetalningsMetod>()
				.ToTable("BetalningsMetod");

			modelBuilder
				.Entity<LeveransMetod>()
				.ToTable("LeveransMetod");

			base.OnModelCreating(modelBuilder);
		}
	}
}