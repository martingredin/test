﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Core.Model;
using Test.Infrastructure.Data;


namespace Test.Tests
{
	[TestFixture]
	public class EfTestContextTests
    {
		[Test]
		public void CreateDB()
		{
			using (var ctx = new EfTestContext())
			{
				ctx.Database.Initialize(false);
			}
		}
    }
}
