﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Core.Model.KundAggregate;
using Test.Core.Model.ProduktAggregate;
using Test.Core.Model.OrderAggregate;
using Test.Core.Model.OrderAggregate;
using Test.Infrastructure.Data;
using Test.Infrastructure.Data.Repositories;
using Test.SharedKernel.Data;

namespace Test.Tests
{
	[TestFixture]
	public class RepositoryIntegrationTests
	{
		[Test]
		public void RepositoryCrudTest()
		{
			var kund1 = Kund.Create("Pelle", "Persson", "Lukasgatan 83", "", "Åmål", "46472", "072-918882", "pelle.persson@hejhopp.se", "pelle", "asd567");
			int kund1Id = Create(kund1);

			var kund2 = Kund.Create("Olle", "Olsson", "Avfolkningsvägen 8A", "2 tr", "Sveg", "19735", "013-197358", "olle.olsson@hejhopp.com", "olle", "asdfg");
			int kund2Id = Create(kund2);

			QuerySingle(kund1);

			QueryAll();

			Update(kund1);

			Delete(kund1);
			Delete(kund2);
		}

		private int Create(Kund kund)
		{
			using (var ctx = new EfTestContext())
			{
				var repository = new EFRepository<Kund>(ctx);
				repository.Add(kund);
				ctx.SaveChanges();

				return kund.Id;
			}
		}

		private void QuerySingle(Kund existerandeKund)
		{
			using (var ctx = new EfTestContext())
			{
				var repository = new EFRepository<Kund>(ctx);
				var inlästKund = repository.GetById(existerandeKund.Id);

				Assert.AreEqual(existerandeKund, inlästKund);
			}
		}

		private void QueryAll()
		{
			using (var ctx = new EfTestContext())
			{
				var repository = new EFRepository<Kund>(ctx);
				var allaKunder = repository.List();

				Assert.IsNotEmpty(allaKunder);
				Assert.IsTrue(allaKunder.Count() > 0);
			}
		}

		private void Update(Kund kund)
		{
			using (var ctx = new EfTestContext())
			{
				ctx.KundCtx.Attach(kund);
				var repository = new EFRepository<Kund>(ctx);
				kund.Efternamn = "Svensson";
				kund.Adressrad2 = "12tr";
				ctx.SaveChanges();

				var återInlästKund = repository.GetById(kund.Id);

				Assert.AreSame(kund, återInlästKund);
				Assert.AreEqual(kund.Efternamn, återInlästKund.Efternamn);
				Assert.AreEqual(kund.Adressrad2, återInlästKund.Adressrad2);
			}
		}

		private void Delete(Kund kund)
		{
			using (var ctx = new EfTestContext())
			{
				ctx.KundCtx.Attach(kund);
				var repository = new EFRepository<Kund>(ctx);
				repository.Remove(kund);
				ctx.SaveChanges();

				var återInlästKund = repository.GetById(kund.Id);

				Assert.AreNotSame(kund, återInlästKund);
				Assert.IsNull(återInlästKund);
			}
		}
	}
}
