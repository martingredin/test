﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Core.ErrorHandling
{
	public class FaultyLoginException : Exception
	{
		private const string message = "Felaktigt användarnamn eller lösenord";

		public FaultyLoginException() :
			base(message) {}
	}
}
