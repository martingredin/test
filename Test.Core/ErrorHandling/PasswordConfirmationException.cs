﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Core.ErrorHandling
{
	public class PasswordConfirmationException : Exception
	{
		private const string message = "Kunde inte skapa användare. Lösenord och bekräftelse matchade inte";
		public PasswordConfirmationException()
			: base(message) { }
	}
}
