﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Core.ErrorHandling
{
	public class UserExistsException : Exception
	{
		private const  string message = "Det finns redan en kund med användarid {0}";
		public UserExistsException(string användarId) :
			base(string.Format(message, användarId))
		{}	
	}
}
