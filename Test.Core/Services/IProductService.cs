﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Core.Model.ProduktAggregate;
using Test.Contracts;

namespace Test.Core.Services
{
	public interface IProductService
	{
		IEnumerable<Produkt> HämtaProdukter();
		Produkt HämtaProdukt(int id);
		void SkapaProdukt(ProduktDto produktDto);
	}
}
