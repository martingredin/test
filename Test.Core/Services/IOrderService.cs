﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Contracts;
using Test.Core.Model.OrderAggregate;

namespace Test.Core.Services
{
	public interface IOrderService
	{
		IEnumerable<BetalningsTyp> HämtaBetalningsTyper();
		IEnumerable<BetalningsMetod> HämtaBetalningsMetoder();
		IEnumerable<LeveransMetod> HämtaLeveransMetoder();
		void SkapaOrder(OrderDto orderDto);
	}
}