﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Core.Model.KundAggregate;
using Test.Contracts;

namespace Test.Core.Services
{
	public interface IKundService
	{
		Kund Registrera(RegistrationDto registrationDto);

		bool LoggaIn(string användarId, string lösenord);

		Kund HämtaKund(string användarId);
	}
}
