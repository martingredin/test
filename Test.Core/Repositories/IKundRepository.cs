﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Core.Model.KundAggregate;
using Test.SharedKernel.Data;

namespace Test.Core.Repositories
{
	public interface IKundRepository : IRepository<Kund>
	{
		Kund GetByAnvändarId(string användarId);
	}
}