﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Core.Model.ProduktAggregate;
using Test.SharedKernel.Data;

namespace Test.Core.Repositories
{
	public interface IProduktRepository : IRepository<Produkt>
	{
		
	}
}
