﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Core.Model.OrderAggregate;
using Test.SharedKernel.Data;

namespace Test.Core.Repositories
{
	public interface IOrderRepository
	{
		IEnumerable<OrderRad> GetOrderRaderByOrderId(int orderId);
		Order GetOrderHuvudByOrderRadId(int orderRadId);

		IEnumerable<BetalningsTyp> GetBetalningsTyper();
		BetalningsMetod GetBetalningsMetod(int betalningsMetodId);
		IEnumerable<BetalningsMetod> GetBetalningsMetoder();
		LeveransMetod GetLeveransMetod(int leveransMetodId);
		IEnumerable<LeveransMetod> GetLeveransMetoder();

		void Add(Order orderHuvud, IEnumerable<OrderRad> orderRader);
	}
}