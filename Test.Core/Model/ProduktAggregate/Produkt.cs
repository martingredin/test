﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Core.Model.ProduktAggregate
{
	public class Produkt
	{
		public int Id { get; private set; }
		public string Namn { get; set; }
		public string Tillverkare { get; set; }
		public decimal Pris { get; set; }

		public Produkt() {}

		public Produkt(int id) 
		{
			Id = id;
		}
	}
}
