﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Core.Model.OrderAggregate;
using Test.SharedKernel.Data;

namespace Test.Core.Model.OrderAggregate
{
	public class Betalning : Entity<int>
	{
		public int OrderId { get; set; }
		public Order Order { get; set; }
		public DateTime BetalningsDatum { get; set; }
		public decimal BetaltBelopp { get; set; }
		
		public Betalning() {}
	}
}
