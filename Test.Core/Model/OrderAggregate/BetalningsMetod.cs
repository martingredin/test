﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.SharedKernel.Data;

namespace Test.Core.Model.OrderAggregate
{
	public class BetalningsMetod : Entity<int>
	{
		public BetalningsMetod() { }

		public string Namn { get; set; }
		public string Data { get; set; }

		public int BetalningsTypId { get; set; }
		public BetalningsTyp BetalningsTyp { get; set; }
	}
}
