﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.SharedKernel.Data;

namespace Test.Core.Model.OrderAggregate
{
	public class LeveransMetod : Entity<int>
	{
		public string Namn { get; set; }
		public decimal Kostnad { get; set; }
		public int LeveransDagar{ get; set; }
	}
}