﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Core.Model.ProduktAggregate;
using Test.SharedKernel.Data;

namespace Test.Core.Model.OrderAggregate
{
	public class OrderRad : Entity<int>
	{
		public Order Order { get; set; }
		public Produkt Produkt { get; set; }

		public int OrderId { get; set; }
		public int ProduktId { get; set; }

		protected OrderRad() {}

		internal static OrderRad Create(Order order, Produkt produkt)
		{
			var orderRad = new OrderRad();
			orderRad.OrderId = order.Id;
			orderRad.Order = order;
			orderRad.ProduktId = produkt.Id;
			orderRad.Produkt = produkt;
			return orderRad;
		}
	}
}
