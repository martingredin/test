﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Core.Model.KundAggregate;
using Test.Core.Model.ProduktAggregate;
using Test.SharedKernel.Data;

namespace Test.Core.Model.OrderAggregate
{
	public class Order : Entity<int>
	{
		public int KundId { get; set; }
		public int BetalningsMetodId { get; set; }
		public int LeveransMetodId { get; set; }

		public Kund Kund { get; set; }
		public BetalningsMetod BetalningsMetod { get; set; }
		public LeveransMetod LeveransMetod { get; set; }

		public virtual IList<OrderRad> OrderRader { get; set; }

		protected Order()
		{ 
			OrderRader = new List<OrderRad>();
		}

		public static Order Create(Kund kund, 
			BetalningsMetod betalningsMetod, 
			LeveransMetod leveransMetod, 
			IEnumerable<Produkt> produkter)
		{
			var order = new Order();
			
			order.KundId = kund.Id;
			order.Kund = kund;

			order.BetalningsMetodId = betalningsMetod.Id;
			order.BetalningsMetod = betalningsMetod;

			order.LeveransMetodId = leveransMetod.Id;
			order.LeveransMetod = leveransMetod;

			foreach (var p in produkter)
			{
				var orderRad = OrderRad.Create(order, p);
				order.OrderRader.Add(orderRad);
			}

			return order;
		}
	}
}
