﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.SharedKernel.Data;

namespace Test.Core.Model.KundAggregate
{
	public class Kund : Entity<int>
	{
		public string Förnamn { get; set; }
		public string Efternamn { get; set; }
		public string Adressrad1 { get; set; }
		public string Adressrad2 { get; set; }
		public string Stad { get; set; }
		public string Postnr { get; set; }

		public string Telnr { get; set; }
		public string Epost { get; set; }

		public string AnvändarId { get; set; }
		public string Lösenord { get; set; }

		protected Kund() {}

		public static Kund Create(string förnamn, string efternamn, string adressrad1, string adressrad2, string stad, string postnr, string telnr, string epost, string användarId, string lösenord)
		{
			var kund = new Kund
			{
				Förnamn = förnamn,
				Efternamn = efternamn,
				Adressrad1 = adressrad1,
				Adressrad2 = adressrad2,
				Stad = stad,
				Postnr = postnr,
				Telnr = telnr,
				Epost = epost,
				AnvändarId = användarId,
				Lösenord = lösenord
			};
			
			return kund;
		}

		public bool Autenticera(string användarId, string lösenord)
		{
			if (användarId == AnvändarId && lösenord == Lösenord)
				return true;

			return false;
		}
	}
}
