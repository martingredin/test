﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Contracts
{
	public class OrderDto
	{
		public string AnvändarId { get; set; }
		public string Lösenord { get; set; }

		public int BetalningsMetodId { get; set; }
		public int LeveransMetodId { get; set; }

		public int[] Produkter { get; set; }
	}
}
