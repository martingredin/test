﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test.Contracts
{
	public class ProduktDto
	{
		public int Id { get; set; }
		public string Namn { get; set; }
		public string Tillverkare { get; set; }
		public decimal Pris { get; set; }
	}
}