﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Contracts
{
	public class SelectOptionDto
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Data { get; set; }
	}
}
