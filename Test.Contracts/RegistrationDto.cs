﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test.Contracts
{
	public class RegistrationDto
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string AddressLine1 { get; set; }
		public string AddressLine2 { get; set; }
		public string City { get; set; }
		public string ZipCode { get; set; }
		public string PhoneNr { get; set; }
		public string EmailAddress { get; set; }

		public string UserId { get; set; }
		public string Password { get; set; }
		public string ConfirmPassword { get; set; }
	}
}