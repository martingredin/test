﻿using AutoMapper;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Test.Core.Model.KundAggregate;
using Test.Core.Model.ProduktAggregate;
using Test.Contracts;
using Test.Web.Providers;
using System.Web.Caching;
using Test.Core.Services;

[assembly: OwinStartupAttribute(typeof(Test.Web.Startup))]
namespace Test.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
			AreaRegistration.RegisterAllAreas();
			GlobalConfiguration.Configure(WebApiConfig.Register);
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);
			OAuthConfig.RegisterOAuth(app);

			var config = new HttpConfiguration();
			WebApiConfig.Register(config);
			DiIocConfig.RegisterDiIoc(app, config);
			app.UseWebApi(config);

			AutomapConfig.RegisterMappings();

			CacheConfig.RegisterCaches();
        }
    }
}