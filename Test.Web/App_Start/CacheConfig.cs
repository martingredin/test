﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SimpleInjector.Extensions.ExecutionContextScoping;
using Test.Core.Services;
using Test.Contracts;
using Test.Core.Model.ProduktAggregate;
using AutoMapper;
using System.Web.Caching;

namespace Test.Web
{
	public class CacheConfig
	{
		public const string FrontpageProdukterCacheKey = "FrontpageProdukter";
		public const string BetTyperCacheKey = "BetTyper";
		public const string BetMetoderCacheKey = "BetMetoder";
		public const string LevMetoderCacheKey = "LevMetoder";

		public static void RegisterCaches()
		{
			using (DiIocConfig.Container.BeginExecutionContextScope())
			{
				var productService = DiIocConfig.Container.GetInstance<IProductService>();
				var frontpageProdukter = productService.HämtaProdukter().ToList();
				var frontpageProduktDtoer = Mapper.Map<List<Produkt>, List<ProduktDto>>(frontpageProdukter);
				HttpRuntime.Cache.Insert(
					FrontpageProdukterCacheKey, 
					frontpageProduktDtoer, 
					null, 
					Cache.NoAbsoluteExpiration,
					TimeSpan.FromMinutes(20),
					FrontpageProdukterUpdateCB);

				var orderService = DiIocConfig.Container.GetInstance<IOrderService>();
				IEnumerable<SelectOptionDto> betTyper = (from bt in orderService.HämtaBetalningsTyper()
														 select new SelectOptionDto { Id = bt.Id, Name = bt.Namn })
														 .ToArray();
				HttpRuntime.Cache.Insert(
					BetTyperCacheKey, 
					betTyper, 
					null, 
					Cache.NoAbsoluteExpiration,
					TimeSpan.FromHours(12),
					BetTyperUpdateCB);

				IEnumerable<SelectOptionDto> betMetoder = (from bt in orderService.HämtaBetalningsMetoder()
														   select new SelectOptionDto { Id = bt.Id, Name = bt.Namn, Data = bt.BetalningsTypId.ToString() })
														   .ToArray();
				HttpRuntime.Cache.Insert(
					BetMetoderCacheKey, 
					betMetoder,
					null, 
					Cache.NoAbsoluteExpiration,
					TimeSpan.FromHours(12),
					BetMetoderUpdateCB);

				IEnumerable<SelectOptionDto> levMetoder = (from bt in orderService.HämtaLeveransMetoder()
														   select new SelectOptionDto { Id = bt.Id, Name = bt.Namn, Data = bt.LeveransDagar.ToString() })
														   .ToArray();
				HttpRuntime.Cache.Insert(
					LevMetoderCacheKey, 
					levMetoder,
					null, 
					Cache.NoAbsoluteExpiration,
					TimeSpan.FromHours(12),
					LevMetoderUpdateCB);
			}
		}

		private static void FrontpageProdukterUpdateCB(string key, CacheItemUpdateReason reason, out object value, out CacheDependency dependency, out DateTime exipriation, out TimeSpan slidingExpiration)
		{
			dependency = null;
			exipriation = Cache.NoAbsoluteExpiration;
			slidingExpiration = TimeSpan.FromSeconds(20);

			using (DiIocConfig.Container.BeginExecutionContextScope())
			{
				var productService = DiIocConfig.Container.GetInstance<IProductService>();
				var frontpageProdukter = productService.HämtaProdukter().ToList();
				value = Mapper.Map<List<Produkt>, List<ProduktDto>>(frontpageProdukter);
			}
		}

		private static void BetTyperUpdateCB(string key, CacheItemUpdateReason reason, out object value, out CacheDependency dependency, out DateTime exipriation, out TimeSpan slidingExpiration)
		{
			dependency = null;
			exipriation = Cache.NoAbsoluteExpiration;
			slidingExpiration = TimeSpan.FromSeconds(20);

			using (DiIocConfig.Container.BeginExecutionContextScope())
			{
				var orderService = DiIocConfig.Container.GetInstance<IOrderService>();
				value = (from bt in orderService.HämtaBetalningsTyper()
						 select new SelectOptionDto { Id = bt.Id, Name = bt.Namn })
						 .ToArray();
			}
		}

		private static void BetMetoderUpdateCB(string key, CacheItemUpdateReason reason, out object value, out CacheDependency dependency, out DateTime exipriation, out TimeSpan slidingExpiration)
		{
			dependency = null;
			exipriation = Cache.NoAbsoluteExpiration;
			slidingExpiration = TimeSpan.FromSeconds(20);

			using (DiIocConfig.Container.BeginExecutionContextScope())
			{
				var orderService = DiIocConfig.Container.GetInstance<IOrderService>();
				value = (from bt in orderService.HämtaBetalningsMetoder()
						 select new SelectOptionDto { Id = bt.Id, Name = bt.Namn, Data = bt.BetalningsTypId.ToString() })
						 .ToArray();
			}
		}

		private static void LevMetoderUpdateCB(string key, CacheItemUpdateReason reason, out object value, out CacheDependency dependency, out DateTime exipriation, out TimeSpan slidingExpiration)
		{
			dependency = null;
			exipriation = Cache.NoAbsoluteExpiration;
			slidingExpiration = TimeSpan.FromSeconds(20);

			using (DiIocConfig.Container.BeginExecutionContextScope())
			{
				var orderService = DiIocConfig.Container.GetInstance<IOrderService>();
				value = (from bt in orderService.HämtaLeveransMetoder()
						 select new SelectOptionDto { Id = bt.Id, Name = bt.Namn, Data = bt.LeveransDagar.ToString() })
						 .ToArray();
			}
		}
	}
}