﻿using Owin;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using Test.Core.Model.ProduktAggregate;
using Test.Core.Repositories;
using Test.Core.Services;
using Test.Infrastructure.Data;
using Test.Infrastructure.Data.Repositories;
using Test.SharedKernel.Data;
using Test.Web.Controllers.Api;
using Test.Web.Services;

namespace Test.Web
{
	public class DiIocConfig
	{
		public static Container Container { get; private set; }

		public static void RegisterDiIoc(IAppBuilder app, HttpConfiguration config)
		{
			// Create the container as usual.
			Container = new Container();

			// Register your types, for instance using the RegisterWebApiRequest extension from the integration package:
			Container.RegisterWebApiRequest<IKundService, KundService>();

			Container.RegisterWebApiRequest<IOrderService, OrderService>();
			Container.RegisterWebApiRequest<IProductService, ProductService>();

			Container.RegisterWebApiRequest<IKundRepository, KundRepository>();
			Container.RegisterWebApiRequest<IOrderRepository, OrderRepository>();
			Container.RegisterWebApiRequest<IProduktRepository, ProduktRepository>();

			Container.RegisterWebApiRequest<EfTestContext>();

			// This is an extension method from the integration package.
			Container.RegisterWebApiControllers(GlobalConfiguration.Configuration);

			Container.Verify();

			config.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(Container);
		}
	}
}