﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Test.Contracts;
using Test.Core.Model.KundAggregate;
using Test.Core.Model.ProduktAggregate;

namespace Test.Web
{
	public class AutomapConfig
	{
		public static void RegisterMappings()
		{
			Mapper.CreateMap<Kund, RegistrationDto>()
				.ForMember(dest => dest.AddressLine1, opt => opt.MapFrom(src => src.Adressrad1))
				.ForMember(dest => dest.AddressLine2, opt => opt.MapFrom(src => src.Adressrad2))
				.ForMember(dest => dest.City, opt => opt.MapFrom(src => src.Stad))
				.ForMember(dest => dest.EmailAddress, opt => opt.MapFrom(src => src.Epost))
				.ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.Förnamn))
				.ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.Efternamn))
				.ForMember(dest => dest.Password, opt => opt.MapFrom(src => src.Lösenord))
				.ForMember(dest => dest.PhoneNr, opt => opt.MapFrom(src => src.Telnr))
				.ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.AnvändarId))
				.ForMember(dest => dest.ZipCode, opt => opt.MapFrom(src => src.Postnr));

			Mapper.CreateMap<Produkt, ProduktDto>();
		}
	}
}