﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Test.Contracts;
using Test.Core.Model.OrderAggregate;
using Test.Core.Model.ProduktAggregate;
using Test.Core.Repositories;
using Test.Core.Services;
using Test.Infrastructure.Data;
using Test.Infrastructure.Data.Repositories;
using Test.SharedKernel.Data;

namespace Test.Web.Services
{
	public class OrderService : IOrderService
	{
		private IOrderRepository orderRepository;

		public OrderService(IOrderRepository orderRepository)
		{
			this.orderRepository = orderRepository;
		}

		public IEnumerable<BetalningsTyp> HämtaBetalningsTyper()
		{
			return orderRepository.GetBetalningsTyper();
		}

		public IEnumerable<BetalningsMetod> HämtaBetalningsMetoder()
		{
			return orderRepository.GetBetalningsMetoder();
		}

		public IEnumerable<LeveransMetod> HämtaLeveransMetoder()
		{
			return orderRepository.GetLeveransMetoder();
		}

		public void SkapaOrder(OrderDto orderDto)
		{
			using (var ctx = new EfTestContext())
			{
				var kundRepository = new KundRepository(ctx);
				var orderRepository = new OrderRepository(ctx);
				var produktRepository = new EFRepository<Produkt>(ctx);

				var kund = kundRepository.GetByAnvändarId(orderDto.AnvändarId);

				var betalningsMetodId = orderRepository.GetBetalningsMetod(orderDto.BetalningsMetodId);
				var leveransMetod = orderRepository.GetLeveransMetod(orderDto.LeveransMetodId);

				var produktLista = new List<Produkt>();
				foreach (var produktId in orderDto.Produkter)
				{
					var produkt = produktRepository.GetById(produktId);
					produktLista.Add(produkt);
				}

				var order = Order.Create(kund, betalningsMetodId, leveransMetod, produktLista);

				orderRepository.Add(order);
				orderRepository.SaveChanges();
			}
		}
	}
}