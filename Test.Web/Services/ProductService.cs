﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Test.Core.Model.ProduktAggregate;
using Test.Core.Repositories;
using Test.Core.Services;
using Test.Infrastructure.Data;
using Test.Infrastructure.Data.Repositories;
using Test.SharedKernel.Data;
using Test.Contracts;

namespace Test.Web.Services
{
	public class ProductService : IProductService
	{
		private IProduktRepository produktRepository;

		public ProductService(IProduktRepository produktRepository)
		{
			this.produktRepository = produktRepository;
		}

		public IEnumerable<Produkt> HämtaProdukter()
		{
			return produktRepository.List();
		}

		public Produkt HämtaProdukt(int id)
		{
			return produktRepository.GetById(id);
		}

		public void SparaProdukt(Produkt produkt)
		{
			produktRepository.Add(produkt);

		}

		public void SkapaProdukt(ProduktDto produktDto)
		{
			var produkt = new Produkt(produktDto.Id)
			{
				Namn = produktDto.Namn,
				Pris = produktDto.Pris,
				Tillverkare = produktDto.Tillverkare
			};

			produktRepository.Add(produkt);
			produktRepository.SaveChanges();
		}
	}
}