﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Test.Core.ErrorHandling;
using Test.Core.Model.KundAggregate;
using Test.Core.Repositories;
using Test.Core.Services;
using Test.Infrastructure.Data;
using Test.Infrastructure.Data.Repositories;
using Test.SharedKernel.Data;
using Test.Contracts;

namespace Test.Web.Services
{
	public class KundService : IKundService
	{
		private IKundRepository kundRepository;

		public KundService(IKundRepository kundRepository)
		{
			this.kundRepository = kundRepository;
		}

		public Kund Registrera(RegistrationDto registrationDto)
		{
			if (registrationDto.Password != registrationDto.ConfirmPassword)
				throw new PasswordConfirmationException();

			var kundExists = LoggaIn(registrationDto.UserId, registrationDto.Password);
			if (kundExists)
				throw new UserExistsException(registrationDto.UserId);

				var kund =
					Kund.Create(
						registrationDto.FirstName, registrationDto.LastName,
						registrationDto.AddressLine1, registrationDto.AddressLine2,
						registrationDto.City, registrationDto.ZipCode,
						registrationDto.PhoneNr, registrationDto.EmailAddress,
						registrationDto.UserId, registrationDto.Password);

				kundRepository.Add(kund);

				return kund;
		}

		public bool LoggaIn(string användarId, string lösenord)
		{
			var kund = kundRepository.GetByAnvändarId(användarId);

			if (kund != null && kund.Autenticera(användarId, lösenord))
				return true;

			return false;
		}

		public Kund HämtaKund(string användarId)
		{
			var kund = kundRepository.GetByAnvändarId(användarId);
			return kund;
		}
	}
}