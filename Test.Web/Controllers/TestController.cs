﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Test.Web.Controllers
{
	public class TestController : Controller
	{
		public ActionResult About()
		{
			return View();
		}

		public ActionResult FrontPage()
		{
			return View();
		}

		public ActionResult Search()
		{
			return View();
		}

		public ActionResult CheckOutAuth()
		{
			return View();
		}

		[Authorize]
		public ActionResult CheckOutOrder()
		{
			return View();
		}

		public ActionResult CheckOutOrderConfirm()
		{
			return View();
		}
	}
}