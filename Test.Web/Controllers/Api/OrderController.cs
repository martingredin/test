﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Test.Contracts;
using Test.Core.Services;
using Test.Web;
using Test.Web.Services;

namespace Test.Web.Controllers.Api
{
    public class OrderController : ApiController
    {
		private IOrderService orderService;

		public OrderController(IOrderService orderService)
		{
			this.orderService = orderService;
		}

		[HttpGet]
		[ActionName("GetBetalningsTyper")]
		[Authorize]
		public IEnumerable<SelectOptionDto> GetBetalningsTyper()
		{
			var selectOptions = HttpRuntime.Cache[CacheConfig.BetTyperCacheKey] as IEnumerable<SelectOptionDto>;

			return selectOptions;
		}

		[HttpGet]
		[ActionName("GetBetalningsMetoder")]
		[Authorize]
		public IEnumerable<SelectOptionDto> GetBetalningsMetoder()
		{
			IEnumerable<SelectOptionDto> selectOptions = HttpRuntime.Cache[CacheConfig.BetMetoderCacheKey] as IEnumerable<SelectOptionDto>;

			return selectOptions;
		}

		[HttpGet]
		[ActionName("GetLeveransMetoder")]
		[Authorize]
		public IEnumerable<SelectOptionDto> GetLeveransMetoder()
		{
			IEnumerable<SelectOptionDto> selectOptions = HttpRuntime.Cache[CacheConfig.LevMetoderCacheKey] as IEnumerable<SelectOptionDto>;

			return selectOptions;
		}

		[HttpPost]
		[ActionName("SkapaOrder")]
		[Authorize]
		public IHttpActionResult SkapaOrder(OrderDto orderDto)
		{
			try
			{
				orderService.SkapaOrder(orderDto);
				return Ok();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError(ex.ToString(), ex.Message);
				return BadRequest(ModelState);
			}
		}
    }
}