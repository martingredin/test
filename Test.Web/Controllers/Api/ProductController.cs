﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Test.Contracts;
using Test.Core.Model.ProduktAggregate;
using Test.Core.Services;
using Test.Infrastructure.Data;
using Test.Infrastructure.Data.Repositories;
using Test.Web;
using Test.Web.Services;

namespace Test.Web.Controllers.Api
{
	public class ProductController : ApiController
    {
		private IProductService productService;

		public ProductController(IProductService productService)
		{
			this.productService = productService;
		}

		[HttpGet]
		public IEnumerable<ProduktDto> GetProdukter()
		{
			var produktDtoer = HttpRuntime.Cache[CacheConfig.FrontpageProdukterCacheKey] as IEnumerable<ProduktDto>;

			return produktDtoer;
		}

		[HttpGet]
		public ProduktDto GetProduktById(int id)
		{
			var produkt = productService.HämtaProdukt(id);

			ProduktDto produktDto = Mapper.Map<Produkt, ProduktDto>(produkt);
			
			return produktDto;
		}

		[HttpPost]
		public void SetProdukt(ProduktDto produktDto)
		{
			productService.SkapaProdukt(produktDto);
		}
	}
}