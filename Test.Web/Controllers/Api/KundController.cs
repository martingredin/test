﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Security;
using Test.Core.ErrorHandling;
using Test.Core.Model.KundAggregate;
using Test.Core.Services;
using Test.Contracts;
using Test.Web.Services;

namespace Test.Web.Controllers.Api
{
    public class KundController : ApiController
    {
		private IKundService kundService;

		public KundController(IKundService kundService)
		{
			this.kundService = kundService;
		}

		[HttpPost]
		[AllowAnonymous]
		public IHttpActionResult Registrera([FromBody]RegistrationDto registationDto)
		{
			try
			{
				var kund = kundService.Registrera(registationDto);

				if (kund == null)
					return BadRequest();

				return Ok();
			}
			catch (UserExistsException uex)
			{
				ModelState.AddModelError(uex.ToString(), uex.Message);
				return BadRequest(ModelState);
			}
			catch (Exception ex)
			{
				ModelState.AddModelError(ex.ToString(), ex.Message);
				return BadRequest(ModelState);
			}
		}

		[HttpGet]
		public RegistrationDto GetAdress(string userId)
		{
			var kund = kundService.HämtaKund(userId);

			if (kund == null)
				throw new HttpResponseException(HttpStatusCode.NotFound);

			RegistrationDto registrationDto = Mapper.Map<Kund, RegistrationDto>(kund);

			return registrationDto;
		}
    }
}