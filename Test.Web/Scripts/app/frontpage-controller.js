﻿angular
    .module('TestApp')
    .controller('FrontPageController', function ($scope, $http, shoppingcartService, cfg) {
        $scope.produkter = {};
        $scope.error = {};

        $http
            .get(cfg.produktUrl)
            .success(function (data) {
                $scope.produkter = data;
            })
            .error(function (error) {
                $scope.error = error;
            });

        $scope.buyProdukt = function (produkt) {
        	shoppingcartService.addToCart(produkt);
        }
    });
