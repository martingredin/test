﻿angular
    .module('TestApp')
	.controller('CheckOutAuthController', ['$scope', '$location', '$timeout', 'authService', function ($scope, $location, $timeout, authService) {
			$scope.loginMessage = '';
			$scope.signUpMessage = '';

			$scope.loginData = {
				UserId: "",
				Password: ""
			};

			$scope.registrationData = {
				FirstName: "",
				LastName: "",
				AddressLine1: "",
				AddressLine2: "",
				City: "",
				ZipCode: "",
				PhoneNr: "",
				EmailAddress: "",

				UserId: "",
				Password: "",
				ConfirmPassword: ""
			};

			$scope.login = function (userId, password) {
				authService
					.login(userId, password)
					.then(function (response) {
						$location.path('/order');
				},
				function (err) {
					$scope.loggedInSuccessfully = false;
				 	$scope.loginMessage = err.error_description;
				});
			};

			$scope.signUp = function () {
				authService
					.saveRegistration($scope.registrationData)
					.then(function (response) {
						$scope.savedSuccessfully = true;
						$scope.signUpMessage = "User has been registered successfully, you are now logged in and will be redicted to the order page.";
						$scope.login($scope.registrationData.UserId, $scope.registrationData.Password);
					},
					function (err) {
						$scope.savedSuccessfully = false;
						$scope.signUpMessage = "Failed to register user due to:" + err.error_description;
					});
				};

			var startTimer = function () {
				var timer = $timeout(function () {
					$timeout.cancel(timer);
					$scope.loginData.UserId = $scope.regLoginData.UserId;
					$scope.loginData.Password = $scope.regLoginData.Password;
					$scope.login();
				}, 2000);
			}
	}]);