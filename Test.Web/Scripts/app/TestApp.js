﻿angular
    .module('TestApp', ['ngRoute', 'ngSanitize', 'ShoppingCartModule', 'AuthenticationModule', 'LocalStorageModule'])
	.constant('cfg', {
		loginUrl: "http://localhost:50424/token",
		produktUrl: "http://localhost:50424/Api/Product/GetProdukter",
		regUrl: "http://localhost:50424/Api/Kund/Registrera",
		getAdressUrl: "http://localhost:50424/Api/Kund/GetAdress",

		betTypUrl: "http://localhost:50424/Api/Order/GetBetalningsTyper",
		betMetodUrl: "http://localhost:50424/Api/Order/GetBetalningsMetoder",
		levMetodUrl: "http://localhost:50424/Api/Order/GetLeveransMetoder",
		skapaOrderUrl: "http://localhost:50424/Api/Order/SkapaOrder"
	})
    .config(function ($routeProvider, $locationProvider, $httpProvider) {
    	$routeProvider.when("/about", {
    		templateUrl: "Test/About"
    	});

    	$routeProvider.when("/search", {
    		templateUrl: "Test/Search"
    	});

    	$routeProvider.when("/auth", {
    		templateUrl: "Test/CheckOutAuth"
    	});

    	$routeProvider.when("/order", {
    		templateUrl: "Test/CheckOutOrder"
    	});

    	$routeProvider.when("/confirmation", {
    		templateUrl: "Test/CheckOutOrderConfirm"
    	});

    	$routeProvider.otherwise({
    		templateUrl: "Test/FrontPage"
    	});

    	$locationProvider.html5Mode(false).hashPrefix('!');

    	$httpProvider.interceptors.push('authInterceptorService');
    });