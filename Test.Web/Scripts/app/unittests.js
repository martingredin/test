﻿/// <reference path="../jasmine.js" />
/// <reference path="../underscore.js" />
/// <reference path="../angular.js" />
/// <reference path="../angular-route.js" />
/// <reference path="../angular-mocks.js" />
/// <reference path="../angular-sanitize.js" />
/// <reference path="../angular-local-storage.js" />
/// <reference path="testapp.js" />
/// <reference path="shoppingcart.js" />
/// <reference path="authentication.js" />
describe('shoppingcartServiceTest', function () {
	beforeEach(module('TestApp'));

	var shoppingcartService;

	beforeEach(inject(function (_shoppingcartService_) {
		shoppingcartService = _shoppingcartService_;
	}));

	it('add to shopping cart, check no items', function () {
		shoppingcartService.clearCart();
		var produkt =
			{
				"id": "2",
				"namn": "Pillimoj",
				"pris": "55",
				"tillverkare": "Stark Industries"
			};
		shoppingcartService.addToCart(produkt);
		expect(shoppingcartService.getCartItems().length).toEqual(1);
	});
});
