﻿angular
    .module('ShoppingCartModule', [])
    .factory('shoppingcartService', function () {
    	var shoppingcartService = {};
    	var items = [];

    	shoppingcartService.addToCart = function (itemToAdd) {
    		var existeingItem = _.find(items, function (item) {
    			return item.id == itemToAdd.id;
    		});

    		if (_.isUndefined(existeingItem)) {
    			items.push({
    				antal: 1,
    				id: itemToAdd.id,
    				namn: itemToAdd.namn,
    				pris: itemToAdd.pris
    			});
    		} else {
    			existeingItem.antal += 1;
    		}
    	};

    	shoppingcartService.removeFromCart = function (itemToRemove) {
    		_.reject(items, function (item) {
    			item.id == itemToRemove.id;
    		});
    	};

    	shoppingcartService.clearCart = function () {
    		items.length = 0;
    	}

    	shoppingcartService.getCartItems = function () {
    		return items;
    	};

    	shoppingcartService.calcNoArticles = function () {
    		if (_.isEmpty(items))
    			return 0;

    		var antal = 0;
    		_.each(items, function (item) {
    			antal += item.antal;
    		});
    		return antal;
    	};

    	shoppingcartService.calcTotSum = function () {
    		if (_.isEmpty(items))
    			return 0;

    		var summa = 0;
    		_.each(items, function (item) {
    			summa += (item.antal * item.pris);
    		});
    		return summa;
    	}

    	return shoppingcartService;
    })
	.directive("shoppingcartPopover", function ($compile, $location, shoppingcartService, authService) {
		return function (scope, element, attrs) {
			scope.cartItems = shoppingcartService.getCartItems();
			scope.antalArtiklar = shoppingcartService.calcNoArticles();
			scope.summaArtiklar = shoppingcartService.calcTotSum();

			scope.$watch(function () {
				return shoppingcartService.calcNoArticles();
			},
			function (newValue) {
				scope.antalArtiklar = shoppingcartService.calcNoArticles();
				scope.summaArtiklar = shoppingcartService.calcTotSum();
			});

			scope.orderFlow = 
				function () {
					if (authService.isAuthenticated())
						$location.path("/order");
					else
						$location.path("/auth");
				}

			var htmlTemplate = [
				"<table class='table'>",
					"<thead><tr><th>Antal</th><th>Namn</th><th>Pris</th></tr></thead>",
					"<tbody><tr ng-repeat='item in cartItems'><td>{{ item.antal }}</td><td>{{ item.namn }}</td><td>{{ item.pris }}</td></tr></tbody>",
				"</table>",
				"<div style='margin-top:10px'>",
					"<span>Antal artiklar: {{ antalArtiklar }}</span>",
				"</div>",
				"<div style='margin-top:2px'>",
					"<span>Summa: {{ summaArtiklar | currency }}</span>",
				"</div>",
				"<div style='margin-top: 5px; margin-bottom: 25px'>",
					"<a class='btn btn-primary btn-xs' ng-click='orderFlow()' role='button' style='float:right'>Gå till kassan</a>",
				"</div>"
			].join("");

			var htmlTable = $compile(htmlTemplate)(scope);
			$(element).popover({
				title: "Innehåll",
				content: htmlTable,
				html: true,
				placement: "bottom"
			});
		}
	})
	.directive("shoppingcartPayment", function ($compile, shoppingcartService) {
		return function (scope, element, attrs) {
			this.cartItems = shoppingcartService.getCartItems();
			this.antalArtiklar = shoppingcartService.calcNoArticles();
			this.summaArtiklar = shoppingcartService.calcTotSum();

			var htmlTemplate = [
					"<table class='table'>",
						"<thead><tr><th>Antal</th><th>Namn</th><th>Pris</th></tr></thead>",
						"<tbody><tr ng-repeat='item in cartItems'><td>{{ item.antal }}</td><td>{{ item.namn }}</td><td>{{ item.pris }}</td></tr></tbody>",
					"</table>",
					"<div style='margin-top:10px'>",
						"<span>Antal artiklar: {{ antalArtiklar }}</span>",
					"</div>",
					"<div style='margin-top:2px'>",
						"<span>Summa: {{ summaArtiklar | currency }}</span>",
					"</div>"
			].join("");

			var htmlTable = $compile(htmlTemplate)(scope);
			element.append(htmlTable);
		}
	});