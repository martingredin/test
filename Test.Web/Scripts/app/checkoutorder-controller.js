﻿angular
    .module('TestApp')
	.controller('CheckOutOrderController', function ($scope, $location, $http, $document, authService, shoppingcartService, cfg) {
		$scope.betTyp = {};
		$scope.valdBetTyp = "";

		var betmetoder = {};
		$scope.betMetod = {};
		$scope.valdBetMetod = {};

		$scope.levMetoder = {};
		$scope.valdLevMetod = {};
		$scope.levTid = "";

		$scope.registrationData = {
			FirstName: "",
			LastName: "",
			AddressLine1: "",
			AddressLine2: "",
			City: "",
			ZipCode: "",
			PhoneNr: "",
			EmailAddress: ""
		};

		$scope.error = "";

		$http
			.get(cfg.betTypUrl)
			.success(function (data) {
				$scope.betTyp = data;
			})
			.error(function (error, status) {
				$scope.error = error;
			});

		$http
			.get(cfg.betMetodUrl)
			.success(function (data) {
				betmetoder = data;
			})
			.error(function (error, status) {
				$scope.error = error;
			});

		$http
			.get(cfg.levMetodUrl)
			.success(function (data) {
				$scope.levMetoder = data;
			})
			.error(function (error, status) {
				$scope.error = error;
			});

		$http
			.get(cfg.getAdressUrl, { params: { userId: authService.getUserName() } })
			.success(function (data) {
				$scope.registrationData.AddressLine1 = data.addressLine1;
				$scope.registrationData.AddressLine2 = data.addressLine2;
				$scope.registrationData.City = data.city;
				$scope.registrationData.ZipCode = data.zipCode;

				$scope.registrationData.PhoneNr = data.phoneNr;
				$scope.registrationData.EmailAddress = data.emailAddress;
			})
			.error(function (error, status) {
				$scope.error = error;
			});

		$scope.disableSendButton = function () {
			if (_.isEmpty($scope.valdBetMetod) || _.isEmpty($scope.valdLevMetod))
				return true;

			return false;
		}
		
		$scope.hideDagar = function () {
			return _.isNull($scope.valdLevMetod) || _.isEmpty($scope.valdLevMetod);
		}

		$scope.valjBetTyp = function () {
			if (_.isNull($scope.valdBetTyp)) {
				$scope.valdBetMetod = null;
				$scope.betMetod = [];
				return;
			}

			$scope.betMetod = _.filter(betmetoder, function (metod) {
				return metod.data == $scope.valdBetTyp.id;
			});

			if ($scope.valdBetTyp.id == 2) {
				$scope.valdBetMetod = $scope.betMetod[0];
			} else {
				$scope.valdBetMetod = {};
			}
		}

		$scope.sendPayment = function () {
			$scope.order = {
				AnvändarId: "",
				BetalningsMetodId: "",
				LeveransMetodId: "",
				Produkter: []
			};

			$scope.order.AnvändarId = authService.getUserName();
			$scope.order.BetalningsMetodId = $scope.valdBetMetod.id;
			$scope.order.LeveransMetodId = $scope.valdLevMetod.id;

			_.each(shoppingcartService.getCartItems(), function (value, key, list) {
				$scope.order.Produkter.push(value.id);
			});

			$http
				.post(cfg.skapaOrderUrl, $scope.order)
				.success(function (response) {
					$location.path("/confirmation");
					authService.logOut();
					shoppingcartService.clearCart();
				})
				.error(function (error, status) {
					$scope.error = error;
				});
		};
	}
);