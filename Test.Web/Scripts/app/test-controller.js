﻿angular
    .module('TestApp')
    .controller('TestController', function ($scope, $http, $location, authService) {
        $scope.displayAbout = function () {
            $location.path("/about");
        }

        $scope.displaySearch = function () {
        	$location.path("/search");

        	var searchValScope = $scope.searchValIn;
        	var searchValScopeVal = $('#txtSearch').val();
        	var searchValDispVal = $('#searchValDisp').text();

        	$('#searchValDisp').text(searchValScope);
        }

        $scope.emergencyLogout = function () {
        	authService.logOut();
        }
    });