﻿using AutoMapper;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Test.Core.ErrorHandling;
using Test.Core.Model.KundAggregate;
using Test.Core.Services;
using Test.Contracts;
using Test.Web.Services;
using SimpleInjector.Extensions.ExecutionContextScoping;

namespace Test.Web.Providers
{
	public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
	{
		public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
		{
			context.Validated();
		}

		public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
		{
			context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
			IKundService kundService = null;
			bool kundAutentiserad;

			using (DiIocConfig.Container.BeginExecutionContextScope())
			{
				kundService = DiIocConfig.Container.GetInstance<IKundService>();
				kundAutentiserad = kundService.LoggaIn(context.UserName, context.Password);
			}

			if (!kundAutentiserad)
			{
				context.SetError("invalid_grant", "Felaktigt användarnamn eller lösenord");
				return;
			}

			var identity = new ClaimsIdentity(context.Options.AuthenticationType);
			identity.AddClaim(new Claim("sub", context.UserName));
			identity.AddClaim(new Claim("role", "user"));

			context.Validated(identity);
		}
	}
}